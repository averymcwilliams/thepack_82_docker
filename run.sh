#!/usr/bin/env bash

docker buildx build -t odinms_mysql odinms_mysql
docker buildx build -t odinms odinms

docker network rm odinms-network
docker network create odinms-network

docker run \
  --pull never \
  --network=odinms-network \
  --name=odinms-mysql-1 \
  -d \
  -p 3306:3306 \
  -e MYSQL_ROOT_PASSWORD=root \
  -e MYSQL_USER=mysql \
  -e MYSQL_PASSWORD=mysql \
  -e MYSQL_DATABASE=odinms \
  odinms_mysql

while ! docker exec odinms-mysql-1 mysql --user=mysql --password=mysql --silent -e 'status' &> /dev/null; do
  echo 'Waiting for docker MySQL'
  sleep 2
done

# Can change ODINMS_CHANNEL_INTERFACE env variable to local IP

docker run \
  --pull never \
  -it \
  --rm \
  --network=odinms-network \
  --name=odinms-server-1 \
  -e MYSQL_HOST=odinms-mysql-1 \
  -e ODINMS_WORLD_HOST=odinms-server-1 \
  -e ODINMS_CHANNEL_INTERFACE=odinms-server-1 \
  -v $(pwd)/odinms/thepack_82:/odinms/thepack_82 \
  -p 7575:7575 \
  -p 7576:7576 \
  -p 7577:7577 \
  -p 7578:7578 \
  -p 8484:8484 \
  odinms bash

docker rm -f odinms-mysql-1
docker network rm odinms-network
